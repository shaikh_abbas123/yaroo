@extends('layouts.app')

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Quick Example <small>jQuery Validation</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="ajaxForm validate" action="{{route('brand.post')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Enter Title" value="{{ @$data->title }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Desription</label>
                        <input type="text" name="description" class="form-control" id="description" placeholder="description" value="{{ @$data->description }}">
                    </div>
                    
                    <div class="form-group">
                        <label>Category Image</label>
                        @if(!empty(@$data->id) && @$data->id != null)
                        @php($appendedFiles = array())
                        @php($file_path = env('CATEGORY_IMAGES').@$data->image)
                        @if (!is_dir($file_path) && isset(get_headers($file_path)[0]) && @get_headers($file_path)[0] != "HTTP/1.0 404 Not Found")

                        <?php
                        @$file_details = getimagesize(env('BRAND_IMAGES') . @$data->image);
                        $size = File::size(storage_path('brand/') . @$data->image);
                        $appendedFiles[] = array(
                            "name" => @$data->image,
                            "type" => $file_details['mime'],
                            "size" => $size,
                            "file" => $file_path,
                            "data" => array(
                                "url" => $file_path,
                                "image_file_id" => $data->id
                            )
                        );
                        ?>
                        @endif
                        @php($appendedFiles = json_encode($appendedFiles))
                        @endif
                        <input type="file" class="form-control" name="fileToUpload" id="fileUploader" data-fileuploader-files='{!! @$appendedFiles !!}'>
                    </div>

                    <div class="form-group mb-0">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" {{ (!isset($data->id)?"checked":'') }} {{ (@$data->is_active == 1)?"checked":"" }} name="is_active" value="on" class="custom-control-input" id="is_active">
                            <label class="custom-control-label" for="is_active"><a href="#">Active</a></label>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <input name="id" type="hidden" value="{{ @$data->id }}">
                    <input name="type" type="hidden" value="{{ @$type }}">
                    <input name="image_old" type="hidden" value="{{ @$data->image }}">
                    <button class="btn btn-primary ajaxFormSubmitAlter" type="button">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>


@endsection
@section('footer')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#fileUploader').fileuploader({
        changeInput: '<div class="fileuploader-input">' +
            '<div class="fileuploader-input-inner">' +
            '<img src="{{ asset("public/assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png") }}">' +
            '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
            '<p>or</p>' +
            '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
            '</div>' +
            '</div>',
        theme: 'dragdrop',
        limit: 1,
        addMore: false,
        extensions: ['jpg', 'jpeg', 'png'],
        onRemove: function(item) {
            $.post('{{route("brand.image.delete")}}', {
                file: item.name,
                data: {
                    image_file_id: "{{ @$data->id }}",
                    file: item.name,
                    image_post_file_id: item.data.image_file_id,
                    "_token": $('meta[name="csrf-token"]').attr('content')
                }
            });
        },
        captions: {
            feedback: 'Drag and drop files here',
            feedback2: 'Drag and drop files here',
            drop: 'Drag and drop files here'
        },
    });
    $(function() {
        $('.select2').select2()
        $('form.validate').validate({
            rules: {
                title: {
                    required: true,
                },
                description: {
                    required: true,
                },

            },
            messages: {
                title: {
                    required: "Please enter title",
                },
                description: {
                    required: "Please provide description",
                },
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection