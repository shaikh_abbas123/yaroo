@extends('layouts.app')

@section('content')
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Quick Example <small>jQuery Validation</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form class="ajaxForm validate" action="{{route('product.post')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Enter Title" value="{{ @$data->title }}">
                    </div>
                    <div class="form-group">
                        <label for="title">Model</label>
                        <input type="text" name="model" class="form-control" id="model" placeholder="Enter Model" value="{{ @$data->model }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Desription</label>
                        <input type="text" name="description" class="form-control" id="description" placeholder="description" value="{{ @$data->description }}">
                    </div>
                    <div class="form-group">
                        <label>Brand</label>
                        <select class="select2" name="brand_id" data-placeholder="Select Brand" style="width: 100%;">
                            @foreach($brand as $v)
                            <option {{ (@$data->brand_id == $v->id)?"selected":"" }} value="{{ $v->id }}">{{ $v->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select class="select2" name="category_id[]" multiple="multiple" data-placeholder="Select Category" data-dropdown-css-class="select2-purple" style="width: 100%;">
                        @foreach($parent_category as $vp)
                        <option disabled>{{ $vp->title }}</option>
                            @foreach($category as $v)
                            @if($vp->id == $v->parent_id)
                                <?php $selected = ''; ?>
                                @if(isset($data->id))
                                    @foreach(@$data->category as $v1)
                                        @if(@$v1->product_id != NULL)
                                            @if(@$v1->category_id == @$v->id)
                                                <?php $selected = 'selected'; ?>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                                

                                <option <?= $selected; ?> value="{{ $v->id }}">{{ $v->title }}</option>
                                @endif
                            @endforeach
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Discount Type</label>
                        <select name="discount_type" class="form-control select2">
                        <option <?= (@$data->discount_type == 0) ? 'selected' : '' ?> value="0">Percentage</option>
                        <option <?= (@$data->discount_type == 1) ? 'selected' : '' ?> value="1">Amount</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Discount </label>
                        <input type="text" name="discount" class="form-control allow_decimal" value="{{@$data->discount}}">
                    </div>

                    <div class="form-group">
                        <label>Price </label>
                        <input type="text" name="price" class="form-control allow_decimal" value="{{@$data->price}}">
                    </div>

                    <div class="form-group">
                        <label>Inventory </label>
                        <input type="text" name="inventory" class="form-control allow_number" value="{{@$data->inventory}}">
                    </div>
                    <div class="form-group">
                        <label>Product Images</label>
                        @if(!empty(@$data->id) && @$data->id != null)
                        @php($appendedFiles = array())
                        @foreach($images as $img)
                        @php($file_path = env('PRODUCT_IMAGES').@$img['image'])
                        @if (!is_dir($file_path) && isset(get_headers($file_path)[0]) && @get_headers($file_path)[0] != "HTTP/1.0 404 Not Found")

                        <?php
                        @$file_details = getimagesize(env('PRODUCT_IMAGES') . @$img->image);
                        $size = File::size(storage_path('product/') . @$img->image);
                        $appendedFiles[] = array(
                        "name" => @$img->image,
                        "type" => $file_details['mime'],
                        "size" => $size,
                        "file" => $file_path,
                        "data" => array(
                            "url" => $file_path,
                            "image_file_id" => $data->id
                        )
                        );
                        ?>
                        @endif
                        @endforeach
                        @php($appendedFiles = json_encode($appendedFiles))
                        @endif
                        <input type="file" class="form-control" name="fileToUpload" id="fileUploader" multiple="" data-fileuploader-files='{!! @$appendedFiles !!}'>
                    </div>

                    <div class="form-group mb-0">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" {{ (!isset($data->id)?"checked":'') }} {{ (@$data->is_active == 1)?"checked":"" }} name="is_active" value="on" class="custom-control-input" id="is_active">
                            <label class="custom-control-label" for="is_active"><a href="#">Active</a></label>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <input name="id" type="hidden" value="{{ @$data->id }}">
                    <input name="type" type="hidden" value="{{ @$type }}">
                    <button class="btn btn-primary ajaxFormSubmitAlter" type="button">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">

    </div>
    <!--/.col (right) -->
</div>


@endsection
@section('footer')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#fileUploader').fileuploader({
        changeInput: '<div class="fileuploader-input">' +
            '<div class="fileuploader-input-inner">' +
            '<img src="{{ asset("public/assets/admin/myplugin/fileuploader/images/fileuploader-dragdrop-icon.png") }}">' +
            '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
            '<p>or</p>' +
            '<div class="fileuploader-input-button"><span>Browse Files</span></div>' +
            '</div>' +
            '</div>',
        theme: 'dragdrop',
        limit: 4,
        addMore: false,
        extensions: ['jpg', 'jpeg', 'png'],
        onRemove: function(item) {
            $.post('{{route("product.image.delete")}}', {
                file: item.name,
                data: {
                    image_file_id: "{{ @$data->id }}",
                    file: item.name,
                    image_post_file_id: item.data.image_file_id,
                    "_token": $('meta[name="csrf-token"]').attr('content')
                }
            });
        },
        captions: {
            feedback: 'Drag and drop files here',
            feedback2: 'Drag and drop files here',
            drop: 'Drag and drop files here'
        },
    });
    $(function() {
        $('.select2').select2()
        $('form.validate').validate({
            rules: {
                title: {
                    required: true,
                },
                

            },
            messages: {
                title: {
                    required: "Please enter title",
                },
                
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection