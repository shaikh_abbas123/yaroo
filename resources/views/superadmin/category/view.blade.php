@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">DataTable with minimal features & hover style</h3>
    </div>
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Title</th>
                    <th>Image</th>
                    <?php if($type == 'sub'){?>
                        <th>Parent</th>
                    <?php } ?>
                    <th>Description</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php ($count = 1)
                @foreach( $data as $v)
                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ $v->title }}</td>
                    <td class="text-center"><img width="50" src="{{env('CATEGORY_IMAGES')}}{{ (file_exists(storage_path('category/').@$v->image) && @$v->image != '') ? @$v->image : 'dummy.jpeg' }}" /></td>
                    <?php if($type == 'sub'){?>
                        <td>{{ @$v->parent[0]->title }}</td>
                    <?php } ?>
                    <td>{{ $v->description }}</td>
                    <td>{{ ($v->is_active ==1)?'Yes':'No' }}</td>
                    <td>
                    <a href="{{ ($v->parent_id == 0)?route('category.add'):route('sub_category.add')}}/{{$v['id']}}" class="btn-warning btn btn-sm" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
           
        </table>
    </div>
</div>


@endsection
@section('footer')
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>
@endsection