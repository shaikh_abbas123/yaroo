<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('/login', function () {
    return view('auth/login');
});
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//User Permission
Route::get('add-permission', 'SuperAdmin\RoleController@index')->name('add.permission');
Route::get('add-role', 'SuperAdmin\RoleController@add_role')->name('add.role');
Route::get('assign-role', 'SuperAdmin\RoleController@assign_role')->name('assign.role');

// Auth::routes();
Route::group(['middleware' => ['role:SuperAdmin', 'auth']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    // Route::get('/login', 'HomeController@index')->name('dashboard');

    //Category
	Route::group(['prefix' => 'category'], function () {
		Route::get('add/{id?}', 'SuperAdmin\CategoryController@add')->name('category.add');
		Route::post('post', 'SuperAdmin\CategoryController@post')->name('category.post');
		Route::get('/', 'SuperAdmin\CategoryController@index')->name('category.view');
        Route::post('/delete/image/}', 'SuperAdmin\CategoryController@delete_image')->name('category.image.delete');
	});

    //Sub Category
	Route::group(['prefix' => 'sub_category'], function () {
		Route::get('add_sub/{id?}', 'SuperAdmin\CategoryController@add_sub')->name('sub_category.add');
		Route::get('sub_index', 'SuperAdmin\CategoryController@index_sub')->name('sub_category.view');
	});

    //Brand
	Route::group(['prefix' => 'brand'], function () {
		Route::get('add/{id?}', 'SuperAdmin\BrandController@add')->name('brand.add');
		Route::post('post', 'SuperAdmin\BrandController@post')->name('brand.post');
		Route::get('/', 'SuperAdmin\BrandController@index')->name('brand.view');
        Route::post('/delete/image/}', 'SuperAdmin\BrandController@delete_image')->name('brand.image.delete');
	});

    //Product
	Route::group(['prefix' => 'product'], function () {
		Route::get('add/{id?}', 'SuperAdmin\ProductController@add')->name('product.add');
		Route::post('post', 'SuperAdmin\ProductController@post')->name('product.post');
		Route::get('/', 'SuperAdmin\ProductController@index')->name('product.view');
        Route::post('/delete/image/}', 'SuperAdmin\ProductController@delete_image')->name('product.image.delete');
	});
    
});

