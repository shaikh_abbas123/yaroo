<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    public function parent(){
        return $this->hasMany('App\Category', 'id', 'parent_id');
    }
}

