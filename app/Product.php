<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    public function brand(){
        return $this->hasOne('App\Brand', 'id', 'brand_id');
    }
    public function images(){
        return $this->hasMany('App\Image', 'file_id', 'id');
    }
    public function category(){
        return $this->hasMany('App\ProductCategory', 'product_id', 'id');
    }
}

