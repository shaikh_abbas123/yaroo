<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Validator;
use App\Brand;
use App\User;
class BasicController extends Controller
{
    public function __construct()
    {
        // $this->middleware(['auth']);
    }

    public function authenticate(Request $request)
    {
        $userdata = array(
            'email'     => $request->input('email'),
            'password'  => $request->input('password')
        );
        if(Auth::attempt($userdata))
        {
            $token = Str::random(60);

            User::where('id', Auth::user()->id)->update([
                'api_token' => hash('sha256', $token),
            ]);

            $data =  array(
                'token' => $token
            );
            return response()->json(['data'=> $data, 'status'=> true]);
        }else
        {
            
            return response()->json(['data'=> 'Error', 'status'=>false]);
        }
        
    }

    public function get_brands()
    {
        $data = Brand::orderBy('id', 'DESC')->get();
        return response()->json(['data'=>$data, 'status'=>true]);
    } 
}