<?php
namespace App\Http\Controllers\SuperAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Validator;
use App\Product;
use App\ProductCategory;
use App\Category;
use App\Image;
use App\Brand;
class ProductController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Product::orderBy('id', 'DESC')->get();
        return view('superadmin.product.view')->with(['title' => 'Product', 'data' => $data]);
    } 

    public function add(Request $request){
    	$data = "";
    	if(isset($request->id)){
    		$data = Product::with(['images','category'])->where('id', $request->id)->first();
            $images = Image::where([['file_id', '=', $request->id], ['image_type', '=', 'Product']])->get(); 
            
    	}

        $parent_category = Category::where([['parent_id','==','0']])->orderBy('id', 'DESC')->get();
        $category = Category::where([['parent_id','!=','0']])->orderBy('id', 'DESC')->get();
        $brand = Brand::orderBy('id', 'DESC')->get();
        return view('superadmin.product.add')->with(['title' => 'Product', 'data' => $data,'category' => $category,'parent_category' => $parent_category ,'brand' => $brand,'images'=>@$images]);
    }

    public function post(Request $request){
        $request->validate([
            'title' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
        	if(Product::where('title', $request->title)->where('id', '!=', $request->id)->count() > 0){
	        	return ["error" => "Title is alredy in used."];
	        }

	        $data = Product::where('id', $request->id)->update([
	        	'title' => $request->title,
                'model' => $request->model,
	        	'description' => $request->description,
                'brand_id' => $request->brand_id,
                'discount_type' => $request->discount_type,
                'discount' => $request->discount,
                'price' => $request->price,
                'inventory' => $request->inventory,
                'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'updated_by' => Auth::user()->id,
	        ]);

            if(isset($_FILES['fileToUpload']['name'][0])){
                for($f=0; $f< (sizeof($_FILES['fileToUpload']['name']) -1); $f++){
                    $filename  = basename($_FILES['fileToUpload']['name'][$f]);
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    $new       = pathinfo($filename)['filename']."_".time().'.'.$extension ;
                    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$f], storage_path('product')."/{$new}");
                    $img = new Image();
                    $img->image_type = 'Product';
                    $img->image          = @$new;
                    $img->file_id     = $data->id;
                    $img->save();

                }
            }

        }else{
	        if(Product::where('title', $request->title)->count() > 0){
	        	return ["error" => "Title is alredy in used."];
	        }
            
            $data = new Product();
	        $data->title       = $request->title;
            $data->model       = $request->model;
            $data->description      = $request->description;
            $data->brand_id = $request->brand_id;
            $data->discount_type = $request->discount_type;
            $data->discount = $request->discount;
            $data->price = $request->price;
            $data->inventory = $request->inventory;
            $data->is_active      = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->created_by = Auth::user()->id;
            $data->updated_by = Auth::user()->id;
            $data->save();

            if(isset($_FILES['fileToUpload']['name'][0])){
                for($f=0; $f< (sizeof($_FILES['fileToUpload']['name']) -1); $f++){
                    $filename  = basename($_FILES['fileToUpload']['name'][$f]);
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    $new       = pathinfo($filename)['filename']."_".time().'.'.$extension ;
                    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$f], storage_path('product')."/{$new}");
                    $img = new Image();
                    $img->image_type = 'Product';
                    $img->image          = @$new;
                    $img->file_id     = $data->id;
                    $img->save();

                }
            }

            if(isset($request->category_id)){
                $category = $request->category_id;
                for($k = 0; $k< sizeof($category); $k++)
                {
                    $category_data = new ProductCategory();
                    $category_data->product_id           = $data->id;
                    $category_data->category_id = $request->category_id[$k];
                    $category_data->save();
                }
            }
        }
        return ["success" => "Successfully Added.", "redirect" => route('product.view')];
    }

    

    public function delete_image(Request $request){

        $delete = File::delete(storage_path('product')."/".$request->file);
        Image::where('image', $request->file)->delete();
        return 'true';
    }
    

}

