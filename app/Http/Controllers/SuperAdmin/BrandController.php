<?php
namespace App\Http\Controllers\SuperAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Validator;
use App\Brand;
class BrandController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Brand::orderBy('id', 'DESC')->get();
        return view('superadmin.brand.view')->with(['title' => 'Brand', 'data' => $data]);
    } 

    public function add(Request $request){
    	$data = "";
    	if(isset($request->id)){
    		$data = Brand::where('id', $request->id)->first();
    	}
        return view('superadmin.brand.add')->with(['title' => 'Brand', 'data' => $data]);
    }

    public function post(Request $request){
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        if(isset($request->id) && !empty($request->id)){
        	if(Brand::where('title', $request->title)->where('id', '!=', $request->id)->count() > 0){
	        	return ["error" => "Title is alredy in used."];
	        }

            if(isset($_FILES['fileToUpload']['name']) && !empty($_FILES['fileToUpload']['name'])){
                    // $delete_old_image = File::delete(storage_path('category')."/".$request->image_old);
                    $filename  = basename($_FILES['fileToUpload']['name']);
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    $new       = pathinfo($filename)['filename']."_".time().'.'.$extension ;
                    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], storage_path('brand')."/{$new}");
                    $image = @$new;
            }
            
	        $data = Brand::where('id', $request->id)->update([
	        	'title' => $request->title,
	        	'description' => $request->description,
                'image' => isset($image)?@$image:$request->image_old,
                'parent_id' => ($request->type == 'sub')?$request->parent_id:'0',
	        	'is_active' => (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0,
                'updated_by' => Auth::user()->id,
	        ]);
        }else{
	        if(Brand::where('title', $request->title)->count() > 0){
	        	return ["error" => "Title is alredy in used."];
	        }
            
            if(isset($_FILES['fileToUpload']['name'])){
                    
                    $filename  = basename($_FILES['fileToUpload']['name']);
                    $extension = pathinfo($filename, PATHINFO_EXTENSION);
                    $new       = pathinfo($filename)['filename']."_".time().'.'.$extension ;
                    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], storage_path('brand')."/{$new}");
                    $image = @$new;
            }
            
            $data = new Brand();
	        $data->title       = $request->title;
            $data->description      = $request->description;
            $data->is_active      = (isset($request->is_active) && $request->is_active == 'on' )? 1 : 0;
            $data->image        = @$image;
            $data->created_by = Auth::user()->id;
            $data->updated_by = Auth::user()->id;
            $data->save();

        }
        return ["success" => "Successfully Added.", "redirect" => route('brand.view')];
    }

    

    public function delete_image(Request $request){
        $delete = File::delete(storage_path('brand')."/".$request->file);
        $data = Brand::where('id', $request->image_file_id)->update([
            'image' => ''
        ]);
        return 'true';
    }
    

}

